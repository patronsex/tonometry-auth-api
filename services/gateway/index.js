const fastify = require('fastify')({
    logger: true
});

const cors = require('fastify-cors');
const body = require('fastify-formbody');
const fistifyHemera = require('fastify-hemera');
const config = require('./config/production');

//for jwt
const jwt = require('jsonwebtoken');
//for jwt end

async function start() {
    await fastify.register(cors);
    await fastify.register(body);

    //интеграци с hemera

    await fastify.register(fistifyHemera, {
        hemera: {
            name: 'gateway',
            logLevel: 'info',
            childLogger: true,
            tag: 'hemera-gateway'
        },
        nats: {
            url: config.nats.url,
        }
    });


    fastify.route({
        method: 'POST',
        url: '/api/v1/auth/register',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'register',
                cmd: 'auth',
                email: req.body.email,
                password: req.body.password
            });

            reply.send(
                response.data
            );
        }
    });

    fastify.route({
        method: 'POST',
        url: '/api/v1/auth/login',
        handler: async (req, reply) => {
            const response = await req.hemera.act({
                topic: 'login',
                cmd: 'auth',
                email: req.body.email,
                password: req.body.password
            });

            reply.send(
                response.data
            );
        }
    });

    fastify.route({
        method: 'GET',
        url: '/api/v1/auth/refresh',
        handler: async (req, reply) => {
            const token = req.query.token || req.headers['x-access-token'];
            const token_refresh = req.query.refresh_token;
            const response = await req.hemera.act({
                topic: 'login',
                cmd: 'refresh',
                token: token,
                refresh_token: token_refresh
            });
            reply.send(response.data);
        }
    });

    fastify.route({
        method: 'GET',
        url: '/api/v1/auth/check',
        handler: async (req, reply) => {
            const token = req.query.token || req.headers['x-access-token'];
            if(token) {
                jwt.verify(token, "mbnsdfz5245xcjkn2453dsvasdlk", function(err, decoded) {
                    if (err) {
                        return reply.code(401).send({"error": true, "message": 'Unauthorized access.' });
                    }
                    else {
                        return reply.send(decoded);
                    }
                });
            } else {
                reply.code(401);
                reply.send({
                    "error": true,
                    "message": 'No token provided.'
                });
            }
        }
    });

    fastify.listen(config.gateway.port, (err, address) => {
        if (err) throw err;
        fastify.log.info(`server listening on ${address}`);
    });

}

module.exports = start;