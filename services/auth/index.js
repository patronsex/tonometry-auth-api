const mysql = require('promise-mysql');
const config = require('./config/production');
const Hemera = require('nats-hemera');
const HemeraJoi = require('hemera-joi');
const nats = require('nats').connect("http://localhost:4222");
const validator = require("email-validator");
const passwordValidator = require('password-validator');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const salt = config.data.salt;
const uuidv4 = require('uuid/v4');

const schema = new passwordValidator();
schema
    .is().min(8)
    .is().max(100)
    .has().uppercase()
    .has().lowercase()
    .has().digits()
    .has().not().spaces();


const setPassword = (password, salt) => {
    return new Promise(resolve => {
        const hash = crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
        return resolve({hash});
    });
};

const validatePassword = (password, salt, hash) => {
    return new Promise(resolve => {
        const local_hash = crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');
        return resolve(local_hash === hash);
    });
};

const generateJWT = (id, email) => {
    return new Promise(resolve => {
        return resolve(jwt.sign({
            email: email,
            id: id,
        }, config.data.jwt_secret, {
            expiresIn: '1s'
        }));
    });
};

const generateRefresh = () => {
    return uuidv4();
};

const decodeToken = (token) => {
    return new Promise(resolve => {
        jwt.verify(token, config.data.jwt_secret, {ignoreExpiration: true}, function (err, decoded) {
            if (err) {
                return resolve({"error": true, "message": 'Unauthorized access.'});
            } else {
                return resolve({"error": false, decoded});
            }
        });
    });
};

const hemera = new Hemera(nats, {
    logLevel: 'info',
    childLogger: true,
    tag: 'hemera-auth'
});

async function start() {
    const con = await mysql.createConnection({
        host: config.data.host,
        port: config.data.port,
        user: config.data.user,
        password: config.data.password,
        database: config.data.database
    });
    //подаждать запуск подключения к базе данных
    hemera.use(HemeraJoi);
    //подаждать пока микросервис себя создаст
    await hemera.ready();
    //создание экземпляра валитора входящих данных
    const Joi = hemera.joi;

    hemera.add({
            topic: 'register',
            cmd: 'auth',
            email: Joi.string().required(),
            password: Joi.string().required(),
        },
        async (req) => {
            if (validator.validate(req.email) && schema.validate(req.password)) {
                const password = await setPassword(req.password, salt);
                try {
                    await con.query("INSERT INTO `users` (email, password) VALUES (?,?)", [req.email, password.hash]);
                    return {register: true, error:  "Successful registration"};
                } catch (e) {
                    return {register: false, error: "Error adding new user to database"};
                }
            } else {
                //Проверка почты на валидацию
                if(!validator.validate(req.email)){
                    return {register: false, error: "Invalid Email"};
                }
                //Если пароль не прошел валидацию, возвращаем ошибку вида "Invalid password: [ 'min', 'uppercase', 'lowercase' ]"
                if(!schema.validate(req.password)){
                    let error_message = (schema.validate(req.password, { list: true }));
                    return {register: false, error: "Invalid password: " + error_message};
                }
            }
        });

    hemera.add({
            topic: 'login',
            cmd: 'refresh',
            token: Joi.string().required(),
            refresh_token: Joi.string().required()
        },
        async (req) => {
            const {token, refresh_token} = req;
            const decoded = await decodeToken(token);
            if (!decoded.error) {
                const query = "SELECT * FROM users WHERE email = ?";
                const users = await con.query(query, [decoded.decoded.email]).catch((e) => {
                    console.log(e);
                    return null;
                });
                if (users && users.length) {0
                    const currentUser = users[0];
                    if (refresh_token === currentUser.refresh_token) {
                        const token = await generateJWT(currentUser.doctor_id, currentUser.email);
                        const refresh_token = generateRefresh();
                        const query = "UPDATE users SET refresh_token = ?";
                        const updated = await con.query(query, [refresh_token]).then(() => true).catch(() => false);
                        if(updated) return {token, refresh_token};
                        else return {error: true, message: "token can not be created"};
                    }
                    else {
                        await con.query('UPDATE users SET refresh_token = null ');
                        return {error: true, message: "different token"};
                    }
                } else {
                    return {error: true, message: "user not found"};
                }
            } else {
                return decoded.message;
            }
        });

    hemera.add({
            topic: 'login',
            cmd: 'auth',
            email: Joi.string().required(),
            password: Joi.string().required(),
        },
        async (req) => {
            if (validator.validate(req.email) && schema.validate(req.password)) {
                try {
                    const getHash = await con.query("SELECT password, doctor_id FROM users WHERE email = ?", [req.email]).catch((e) => {
                        console.log(e);
                        return null;
                    });
                    if (getHash && getHash.length) {
                        const hash = getHash[0].password; // Полученный хэш из базы
                        const doctor_id = getHash[0].doctor_id; // Полученный ID из базы
                        const validPassword = await validatePassword(req.password, salt, hash);
                        if (validPassword) {
                            const jwtKey = await generateJWT(doctor_id, req.email);
                            const refreshKey = await generateRefresh();
                            try {
                                const query = "UPDATE users SET refresh_token = ? WHERE email = ?";
                                await con.query(query, [refreshKey, req.email]);
                            } catch (e) {
                                return {login: false, error: 'token cannot be created'};
                            }
                            return {login: true, token: jwtKey, refresh_token: refreshKey};
                        } else {
                            return {login: false, error: 'Invalid password'};
                        }
                    } else {
                        if(!getHash){
                            return {login: false, error: 'Invalid Hash'};
                        }
                        if(!getHash.length){
                            return {login: false, error: 'Invalid hash length'};
                        }
                    }
                } catch (e) {
                    return {login: false, error: 'Failed to get their database hash'};
                }
            } else {
                //Проверка почты на валидацию
                if(!validator.validate(req.email)){
                    return {login: false, error: 'Invalid Email'};
                }
                //Если пароль не прошел валидацию, возвращаем ошибку вида "Invalid password: [ 'min', 'uppercase', 'lowercase' ]"
                if(!schema.validate(req.password)){
                    let error_message = (schema.validate(req.password, { list: true }));
                    return {login: false, error: 'Invalid password: ' + error_message};
                }
            }
        });
}

module.exports = start;