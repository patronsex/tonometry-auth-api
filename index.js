const gateway = require('./services/gateway');
const authService = require('./services/auth');

(async ()=>{
    await gateway();
    console.log('gateway was started');
    await authService();
    console.log('auth service was started');
})();